"use strict";
/**
 * Adapter class that exposes @elastic/elasticsearch client library functionality.
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Client = require('@elastic/elasticsearch').Client;
var ElasticsearchClientService = /** @class */ (function () {
    // client: any;
    // logger: any;
    /**
     * @constructor
     * @param {{ node, maxRetries, requestTimeout }} config
     * @param {{ info: function, debug: function, error: function }} logger
     */
    function ElasticsearchClientService(_a) {
        var config = _a.config, logger = _a.logger;
        this.client = new Client(config);
        this.logger = logger;
    }
    /**
     * Search for term in ES index
     *
     * @param {string} searchTerm Supplied search term
     * @param {string} index ES index to search
     * @param {string} filters Used to filter results
     * @param {string} searchStrategy Search query strategy
     * @param {from} from Search query strategy
     * @param {size} size Search query strategy
     * @returns {Promise<ApiResponse>}
     */
    ElasticsearchClientService.prototype.search = function (searchTerm, index, filters, searchStrategy, from, size) {
        return __awaiter(this, void 0, void 0, function () {
            var ex_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.client.search({
                                index: index,
                                body: __assign({ from: from, size: size }, this.buildSearchBody(searchTerm, searchStrategy, filters))
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                    case 2:
                        ex_1 = _a.sent();
                        this.logger.error('Error search searching term', {
                            ex: ex_1.message, searchStrategy: searchStrategy, searchTerm: searchTerm, filters: filters, index: index
                        });
                        throw ex_1;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Insert or update document if exists
     *
     * @param {string} id
     * @param {Object} body
     * @param {string} index
     * @returns {Promise<boolean>}
     */
    ElasticsearchClientService.prototype.upsert = function (id, body, index) {
        return __awaiter(this, void 0, void 0, function () {
            var ex_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.client.update({
                                id: id,
                                index: index,
                                type: '_doc',
                                body: {
                                    doc: __assign({}, body),
                                    doc_as_upsert: true,
                                },
                            })];
                    case 1:
                        _a.sent();
                        this.logger.info('Upsert Successful');
                        return [2 /*return*/, true];
                    case 2:
                        ex_2 = _a.sent();
                        this.logger.error('Error creating document', {
                            ex: ex_2.message, id: id, body: body, index: index,
                        });
                        throw ex_2;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Helper method to build search body
     *
     * @param {string} searchTerm
     * @param {{}} filters
     * @param {string} searchStrategy
     * @returns {{}}
     */
    ElasticsearchClientService.prototype.buildSearchBody = function (searchTerm, searchStrategy, filters) {
        if (filters === void 0) { filters = {}; }
        searchStrategy = searchStrategy.replace(/<SEARCH_TERM>/g, searchTerm.toLowerCase());
        var _a = JSON.parse(searchStrategy), query = _a.query, defaultQuery = _a.defaultQuery, availableFilters = _a.availableFilters;
        var generatedQuery = {
            query: {
                bool: {
                    must: searchTerm ? query : defaultQuery,
                    filter: {
                        bool: {
                            must: ElasticsearchClientService.generateFilters(filters, availableFilters)
                        }
                    }
                }
            }
        };
        this.logger.info("Search Query debug", {
            generatedQuery: generatedQuery
        });
        return generatedQuery;
    };
    /**
     * Generate ES query filters
     *
     * @param {{}} filters
     * @param {{}} availableFilters
     * @return {*}
     */
    ElasticsearchClientService.generateFilters = function (filters, availableFilters) {
        var suppliedFilters = Object.keys(filters);
        return availableFilters
            .filter(function (_a) {
            var parameterName = _a.parameterName;
            return suppliedFilters.includes(parameterName);
        })
            .map(function (_a) {
            var _b;
            var field = _a.field, parameterName = _a.parameterName;
            return (Array.isArray(filters[parameterName])
                // Match any of the supplied values if more than one
                ? { bool: { should: filters[parameterName].map(function (value) {
                            var _a;
                            return ({ term: (_a = {}, _a[field] = value, _a) });
                        }) } }
                : ({ term: (_b = {}, _b[field] = filters[parameterName], _b) }));
        });
    };
    return ElasticsearchClientService;
}());
module.exports = ElasticsearchClientService;
