import { ElasticsearchClientService } from '../ElasticsearchClientService'

const indexName = "_index"
const id = "_id"
const searchTerm = ''
const filters = ''
const from = 0 
const size = 10
const searchStrategy = '{"query": "Gold", "defaultQuery": "chiommy", "availableFilters": ["product_name", "product_price"]}'
const body = {}
const expectedResponse = {
  body: {
    acknowledged: true,
    shards_acknowledged: true,
    index: indexName
  },
  statusCode: 200
}
const documentCreateResponse = {
  "_index": indexName,
  "_type": "_doc",
  "_id": id,
  "_version": 1,
  "result": "created",
  "_shards": {
    "total": 2,
    "successful": 1,
    "failed": 0
  },
  "_seq_no": 0,
  "_primary_term": 1
}
const updateResponse = {
  "_index": indexName,
  "_type": "_doc",
  "_id": id,
  "_version": 1,
  "result": "updated",
  "_shards": {
    "total": 2,
    "successful": 1,
    "failed": 0
  },
  "_seq_no": 0,
  "_primary_term": 1
}
const searchResponse = {
  "_index": indexName,
  "_type": "_doc",
  "_id": "__id__",
  "_version": 1,
  "result": "created",
  "_shards": {
    "total": 2,
    "successful": 1,
    "failed": 0
  },
  "_seq_no": 0,
  "_primary_term": 1
}

jest.mock('@elastic/elasticsearch')

let config: {
  node: string
  maxRetries: number,
  requestTimeout: number
}
let logger = console
let esClient: ElasticsearchClientService

describe('ElasticsearchClientService', () => {
  beforeEach(() => {
    config = {
      node: 'http://localhost:9200',
      maxRetries: 2,
      requestTimeout: 5000
    }
    esClient = new ElasticsearchClientService({config, logger})
  })

  it('should successfully create document for supplied index', async () => {
    esClient.client.update = jest.fn().mockReturnValue(updateResponse)
    const response = await esClient.updateDocument(id, indexName, body);
    expect(response).toBeTruthy();
  });

  it("should throw exception when document update is unsuccessful", async () => {
    esClient.client.update = jest.fn().mockImplementation(() => {
      throw new Error('Error creating document')
    })

    try {
      await esClient.updateDocument(id, indexName, body)
    } catch (e) {
      expect(e.message).toBe("Error creating document")
    }
  })

  it("should search a document", async () => {
   
    esClient.client.search = jest.fn().mockReturnValue(searchResponse)
    const response = await esClient.search(indexName, searchTerm, filters,searchStrategy, from, size)
    expect(response).toBeTruthy();
  })

  it("should throw exception when search document is unsuccessful", async () => {
     esClient.client.search = jest.fn().mockImplementation(() => {
       throw new Error('Error searching search term')
     })

     try {
       await esClient.search(indexName, searchTerm, filters,searchStrategy, from, size)
     } catch (e) {
        expect(e.message).toBe('Error searching search term')
     }
  })
})
