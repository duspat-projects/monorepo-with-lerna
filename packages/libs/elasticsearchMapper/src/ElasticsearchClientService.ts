/**
 * Adapter class that exposes @elastic/elasticsearch client library functionality.
 */

import  { Client } from '@elastic/elasticsearch';

interface Logger {
  debug(message: string, context?: any): any
  error(message: string, context?: any) : any
  info(message: string, context?: any) : any
}

interface Config {
   node: string,
   maxRetries: number,
   requestTimeout: number
}

declare type CreateMappingOptions = {
  mappings: {},
  settings: {}
}

class ElasticsearchClientService {
    logger: any;
    client: Client;

  /**
   * @constructor
   * @param {{ node, maxRetries, requestTimeout }} config
   * @param {{ info: function, debug: function, error: function }} logger
   */
  constructor({config, logger}: {config: Config,  logger: Logger} ) {
    this.client = new Client(config);
    this.logger = logger;
  }

  /**
   * Insert or update document if exists
   *
   * @param {string} id
   * @param {Object} body
   * @param {string} index
   * @returns {Promise<boolean>}
   */


  async updateDocument( id: string,  index: string, body: {}) {
    try {
      await this.client.update({
        id,
        index,
        type: '_doc', // required for elasticsearch version 6.x which aws supports
        body: {
          doc: { ...body },
          doc_as_upsert: true,
        },
      });

      this.logger.info('Update Successful');
      return true;
    } catch (ex) {
      this.logger.error('Error creating document', {
        ex: ex.message, id, body, index,
      });
      throw ex;
    }
  }

  /**
   * Search for term in ES index
   *
   * @param {string} searchTerm Supplied search term
   * @param {string} index ES index to search
   * @param {string} filters Used to filter results
   * @param {string} searchStrategy Search query strategy
   * @param {from} from Search query strategy
   * @param {size} size Search query strategy
   * @returns {Promise<ApiResponse>}
   */
  async search(index: string, searchTerm: string,  filters: any, searchStrategy: any, from: number, size: number) {
    try {
      return await this.client.search({
        index,
        body: {
          from,
          size,
          ...this.buildSearchBody(searchTerm, searchStrategy, filters)
        }
      });
    } catch (ex) {
      this.logger.error('Error searching search term', {
        ex: ex.message,
            searchStrategy,
            searchTerm,
            filters,
            index
      });
      throw ex;
    }
  }


  /**
   * Helper method to build search body
   *
   * @param {string} searchTerm
   * @param {{}} filters
   * @param {string} searchStrategy
   * @returns {{}}
   */
  buildSearchBody(searchTerm: string , searchStrategy: any, filters: []) {
    searchStrategy = searchStrategy.replace(/<SEARCH_TERM>/g, searchTerm.toLowerCase());
    const { query, defaultQuery, availableFilters } = JSON.parse(searchStrategy);

    const generatedQuery = {
      query: {
        bool: {
          must: searchTerm ? query : defaultQuery,
          filter : {
            bool : {
              must : ElasticsearchClientService.generateFilters(filters, availableFilters)
            }
          }
        }
      }
    }
    this.logger.info("Search Query debug", {
      generatedQuery
    });

    return generatedQuery;
  }

  /**
   * Generate ES query filters
   *
   * @param {{}} filters
   * @param {{}} availableFilters
   * @return {*}
   */
  static generateFilters(filters: any, availableFilters: any) {
    const suppliedFilters = Object.keys(filters);
    return availableFilters
      .filter(({ parameterName }: {parameterName: string}) => suppliedFilters.includes(parameterName))
      .map(({ field, parameterName }: {parameterName: string, field: string}) => (
        Array.isArray(filters[parameterName])
          // Match any of the supplied values if more than one
          ? { bool: {
                     should: filters[parameterName].map((value: any) => ({ term: { [field]: value } }))
                    }
            }
          : ({ term: {
                     [field]: filters[parameterName]
                     }
             })
      ));
  }
}

export { ElasticsearchClientService }
