import { Client } from "@elastic/elasticsearch"

/**
 * 
 * 
 * @param {string} url 
 */
const BuildElasticSearchClient = (url: string) => {
  return new Client({
    node: url,
  })
}


export { BuildElasticSearchClient }
