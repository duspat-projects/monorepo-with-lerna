// TODO : Delete file from Lib
import { BuildElasticSearchClient } from './BuildElasticSearchClient'
import { ElasticsearchClientService } from './ElasticsearchClientService'

const indexName = 'chiommy_products1';
const indexName2 = 'chiommy_products5';
const indexName3 = 'chiommy_products0';
const id = "456002"
const indexOptions = {
  mappings: {
    "properties": {
      "product_name": {
        "type": "text"
      },
      "product_description": {
        "type": "text"
      },
      "product_type": {
        "type": "text"
      },
      "product_price": {
        "type": "double"
      },
      "product_quantity": {
        "type": "integer"
      },
      "isEnabled": {
        "type": "boolean"
      },
      "created_at": {
        "type": "date",
        "format": "strict_date_optional_time||epoch_millis"
      },
      "updated_at": {
        "type": "date",
        "format": "strict_date_optional_time||epoch_millis"
      }
    }
  },
  settings: {}
}

interface Options {
 mappings: {},
 settings: {}
}
const logger = console
const config = {
  node: 'http://localhost:9200',
  maxRetries: 2,
  requestTimeout: 5000
}
const client = new ElasticsearchClientService({config, logger});

  /**
   * Create or Update an index
   *
   * @param {string} index
   * @param {string} id
   * @param {Object} body
   */

const createIndex = async ( id: string, indexName: string, indexOptions: Options,): Promise<any> => {
  return await client.updateDocument(id, indexName, indexOptions)
}



//createIndex(id, indexName3, indexOptions).then(console.log).catch(console.log)
const updatedDate = new Date().toISOString()
const body = {
  "product_name": `Gold Plated Ball - 10`,
  "product_description": 'Gold Plated Ball Described',
  "product_type": 'Ball',
  "product_price": 10.99,
  "product_quantity": 10,
  "isEnabled": true,
  "created_at":updatedDate,
  "updated_at": updatedDate
}

const body2 = {
  "product_name": `Gold Plated Ball - 5 - Updated`,
  "product_description": 'Gold Plated Ball Described',
  "product_type": 'Ball',
  "product_price": 10.99,
  "product_quantity": 10,
  "isEnabled": true,
  "created_at":updatedDate,
  "updated_at": new Date().toISOString()
}

const body3 = {
  "product_name": `Silver Plated Ball - 10`,
  "product_description": 'Silver Plated Ball Described',
  "product_type": 'Ball',
  "product_price": 12.99,
  "product_quantity": 20,
  "isEnabled": true,
  "created_at":updatedDate,
  "updated_at": new Date().toISOString()
}

const productName = 'Gold'

// client.createDocument(indexName, body, '1234').then(console.log).catch(console.log)
// client.createDocument(indexName2, body, '1234').then(console.log).catch(console.log)
// client.upsertDocument('67801', indexName3, body3,).then(console.log).catch(console.log)
// client.createDocument(indexName, body3, '125445').then(console.log).catch(console.log)
client.search(indexName, 'Gold', productName,'{"query": "Gold", "defaultQuery": "Gold", "availableFilters": ["Gold Ball", "Gold Plated"]}', 0, 10,).then(console.log).catch(console.log)

