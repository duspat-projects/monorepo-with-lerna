#### How to Configure ELS Library

Download Elasticsearch from [here](https://www.elastic.co/downloads/elasticsearch)

Follow the procedure detailed in the link above and install the library
If installed and running properly, navigate to [Here](http://localhost:9200/) to see the library


#### To Use the Library

To use the ELS library follow the steps below

```sh

$ git clone https://bitbucket.org/duspat-projects/monorepo-with-lerna.git

$ cd monorepo-with-lerna

$ npm install 

$ lerna bootstrap

```
Enter elasticsearchMapper Package

```sh

$ cd packages/libs/elasticsearchMapper

```
With elasticsearch server still running on port 9200, Follow the steps below inorder to consume the lib.


```sh

$ yarn run ts-node <path-to-file>

```
Use the index.ts to check if the lib will accept new document, or update an existing one. If it does, it will surely be displayed on the search document using the elasticsearch server url.
 
 > For example if we want to search for
 > document *chiommy_products1* we use the following 
 
```sh

http://localhost:9200/chiommy_products1/_search?

```

#### How to configure search

The search api can be used by including the following in the index.ts 

 - index name
 - search term (supplied search term)
 - filter (supplied filters)
 - search strategy  (consists of query, default query and  filters)
 - from (start index of hits, default is 0)
 - size (the number of hits to be retrieved, default is 10)

 if the above is included correctly, you should be able to search the document using the lib.



