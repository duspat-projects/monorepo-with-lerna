import React from 'react';
import { shallow, mount } from 'enzyme';
import App from '../components/App';

describe("<App/>", () => {

   it("renders without crashing", () => {
      shallow(<App color="Yellow"/>);
      
      const app = mount(<App color="Red"/>);
      expect(app.find(".heading1").text()).toEqual("Welcome to React with Typescript")
    });
});