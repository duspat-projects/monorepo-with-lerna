import { configure } from '@storybook/react';

//configure(require.context('../src', true, /\.stories\.js$/), module);

const req = require.context('../src', true, /\.stories\.tsx?$/)

function loadStories() {
  req.keys().forEach((filename) => req(filename))
}

configure(loadStories, module);