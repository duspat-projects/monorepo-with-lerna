/* eslint-disable */
const jestConfig = require('@duspat/tooling/jest/jest.config');

jestConfig.setupFiles = ['<rootDir>/src/setupTests.ts'];

module.exports = jestConfig;