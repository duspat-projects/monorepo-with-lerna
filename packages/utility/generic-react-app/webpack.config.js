/* eslint-disable */
const config = require('@duspat/tooling/webpack/webpack.config');
const path = require('path');

config.entry = "./src/index.tsx"
config.output = {
    path: path.join(__dirname, '/dist'),
    filename: 'bundle.min.js'
  }
  
module.exports = config;